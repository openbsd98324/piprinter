# piprinter




![](media/piprinter-brother-1.png)


  http://raspbian.raspberrypi.org/raspbian/dists/bookworm-staging/rpi/



PIPRINTER 4.14.98-v7+ #1200 SMP Tue Feb 12 20:27:48 GMT 2019 armv7l

issue: 
Debian GNU/Linux bookworm/sid \n \l



# 2019 

This deb is too old: 

 md5sum  /var/cache/apt/archives/printer-driver-brlaser_3-5_armhf.deb   
 afa302f8aee5d1da385885c95b488e6b  /var/cache/apt/archives/printer-driver-brlaser_3-5_armhf.deb




# 2021/02

So then, upgrade to brlaser 6.1 works fine. 

Working with Laser printer by using the USB Cable !!: 

 lpr -PHL-L2340D-series -o fit-to-page -o media=A4 file.pdf  


USB cable is required.


debian sid has brlaser+


    Linux PIPRINTER 4.14.98-v7+ #1200 SMP Tue Feb 12 20:27:48 GMT 2019 armv7l GNU/Linux
    /lib/modules/ --> 4.14.98+  4.14.98-v7+
    deb http://ftp.debian.org/debian sid main


piprinter-brother-1.txt

 dpkg -l | grep brlaser
 ii  printer-driver-brlaser         6-1                            armhf        printer driver for (some) Brother laser




# 2021/12

An upgrade to 6.2 is now available. 

http://ftp.debian.org/debian sid/main armhf printer-driver-brlaser armhf 6-2 



